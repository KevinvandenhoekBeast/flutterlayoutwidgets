import 'package:flutter/material.dart';
import 'package:flutter_layout_widgets/pages/navigation_stack_page/examples/test_navigation_stack_page_item_a.dart';
import 'package:flutter_layout_widgets/pages/navigation_stack_page/examples/test_navigation_stack_page_item_b.dart';
import 'package:flutter_layout_widgets/pages/navigation_stack_page/extensions/animated_navigation_stack_page.dart';
import 'package:flutter_layout_widgets/pages/navigation_stack_page/navigation_stack_page.dart';
import 'package:flutter_layout_widgets/pages/top_bar_page/test_pages/test_top_bar_page_a.dart';
import 'package:flutter_layout_widgets/pages/top_bar_page/test_pages/test_top_bar_page_b.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var controller = NavigationStackController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedNavigationStackPage(
        controller: controller,
        rootWidget: TestNavigationStackPageItemA(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (controller.parent.presentedPage is TestNavigationStackPageItemA) {
            controller.push(TestNavigationStackPageItemB());
          } else {
            controller.push(TestNavigationStackPageItemA());
          }
        },
        child: Icon(Icons.flip),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
